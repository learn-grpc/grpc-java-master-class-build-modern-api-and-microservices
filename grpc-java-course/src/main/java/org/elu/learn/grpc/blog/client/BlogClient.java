package org.elu.learn.grpc.blog.client;

import com.proto.blog.Blog;
import com.proto.blog.BlogServiceGrpc;
import com.proto.blog.CreateBlogRequest;
import com.proto.blog.CreateBlogResponse;
import com.proto.blog.DeleteBlogRequest;
import com.proto.blog.DeleteBlogResponse;
import com.proto.blog.ListBlogRequest;
import com.proto.blog.ReadBlogRequest;
import com.proto.blog.ReadBlogResponse;
import com.proto.blog.UpdateBlogRequest;
import com.proto.blog.UpdateBlogResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;

public class BlogClient {
    public static void main(String[] args) {
        System.out.println("Hello, I'm a gRPC client for Blog");
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 50053)
            .usePlaintext()
            .build();

        BlogServiceGrpc.BlogServiceBlockingStub blogClient = BlogServiceGrpc.newBlockingStub(channel);

        Blog blog = Blog.newBuilder()
            .setAuthorId("edu")
            .setTitle("Hello world")
            .setContent("This is my first blog")
            .build();

        // create blog
        CreateBlogResponse createResponse = blogClient.createBlog(CreateBlogRequest.newBuilder()
            .setBlog(blog)
            .build());

        System.out.println("Received create blog response");
        System.out.println(createResponse);

        // read blog
        String blogId = createResponse.getBlog().getId();
        ReadBlogResponse readResponse = blogClient.readBlog(ReadBlogRequest.newBuilder()
            .setBlogId(blogId)
            .build());

        System.out.println("Received read blog response");
        System.out.println(readResponse);

        try {
            ReadBlogResponse readResponseException = blogClient.readBlog(ReadBlogRequest.newBuilder()
                .setBlogId("fake-id")
                .build());
            System.out.println(readResponseException);
        } catch (StatusRuntimeException ex) {
            System.out.println(ex.getStatus());
        }
        System.out.println();

        try {
            ReadBlogResponse readResponseNotFound = blogClient.readBlog(ReadBlogRequest.newBuilder()
                .setBlogId("5d429735f1094f3564b28dda")
                .build());
            System.out.println(readResponseNotFound);
        } catch (StatusRuntimeException ex) {
            System.out.println(ex.getStatus());
        }
        System.out.println();

        // update blog
        Blog newBlog = Blog.newBuilder()
            .setId(blogId)
            .setAuthorId("changed author")
            .setTitle("Bye world")
            .setContent("This is an updated blog")
            .build();

        UpdateBlogResponse updateResponse = blogClient.updateBlog(UpdateBlogRequest.newBuilder().setBlog(newBlog).build());
        System.out.println("Update blog");
        System.out.println(updateResponse);

        // delete blog
        DeleteBlogResponse deleteResponse = blogClient.deleteBlog(DeleteBlogRequest.newBuilder().setBlogId(blogId).build());
        System.out.println("Blog was deleted: " + deleteResponse.getDeleted());

        System.out.println("Reading after delete...");
        try {
            ReadBlogResponse resp = blogClient.readBlog(ReadBlogRequest.newBuilder().setBlogId(blogId).build());
            System.out.println(resp);
        } catch (StatusRuntimeException ex) {
            if (ex.getStatus().getCode() == Status.Code.NOT_FOUND) {
                System.out.println("blog is deleted");
            } else {
                System.out.println("exception caught: " + ex.getStatus());
            }
        }

        // list blog
        blogClient.listBlog(ListBlogRequest.newBuilder().build())
            .forEachRemaining(listBlogResponse -> System.out.println(listBlogResponse.getBlog()));
    }
}
