package org.elu.learn.grpc.blog.server;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.proto.blog.Blog;
import com.proto.blog.BlogServiceGrpc;
import com.proto.blog.CreateBlogRequest;
import com.proto.blog.CreateBlogResponse;
import com.proto.blog.DeleteBlogRequest;
import com.proto.blog.DeleteBlogResponse;
import com.proto.blog.ListBlogRequest;
import com.proto.blog.ListBlogResponse;
import com.proto.blog.ReadBlogRequest;
import com.proto.blog.ReadBlogResponse;
import com.proto.blog.UpdateBlogRequest;
import com.proto.blog.UpdateBlogResponse;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import org.bson.Document;
import org.bson.types.ObjectId;

import static com.mongodb.client.model.Filters.eq;

public class BlogServiceImpl extends BlogServiceGrpc.BlogServiceImplBase {
    private MongoClient mongoClient = MongoClients.create("mongodb://localhost:27017");
    private MongoDatabase database = mongoClient.getDatabase("mydb");
    private MongoCollection<Document> collection = database.getCollection("blog");

    @Override
    public void createBlog(CreateBlogRequest request, StreamObserver<CreateBlogResponse> responseObserver) {
        System.out.println("Received create blog request");
        Blog blog = request.getBlog();

        Document doc = new Document("author_id", blog.getAuthorId())
            .append("title", blog.getTitle())
            .append("content", blog.getContent());

        System.out.println("Inserting blog...");
        collection.insertOne(doc);

        String id = doc.getObjectId("_id").toString();
        System.out.println(String.format("Inserted blog with id [%s]", id));

        CreateBlogResponse response = CreateBlogResponse.newBuilder()
            .setBlog(blog.toBuilder().setId(id).build())
            .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void readBlog(ReadBlogRequest request, StreamObserver<ReadBlogResponse> responseObserver) {
        System.out.println("Received read blog request");
        String blogId = request.getBlogId();

        try {
            Blog blog = findById(blogId);
            System.out.println("Blog found, sending response");

            responseObserver.onNext(ReadBlogResponse.newBuilder()
                .setBlog(blog)
                .build());
            responseObserver.onCompleted();
        } catch (StatusRuntimeException ex) {
            responseObserver.onError(ex);
        } catch (Exception ex) {
            responseObserver.onError(Status.NOT_FOUND
                .withDescription("The blog with given id was not found")
                .augmentDescription(ex.getLocalizedMessage())
                .asRuntimeException());
        }
    }

    @Override
    public void updateBlog(UpdateBlogRequest request, StreamObserver<UpdateBlogResponse> responseObserver) {
        System.out.println("Received update blog request");
        Blog blog = request.getBlog();
        String blogId = blog.getId();

        try {
            findById(blogId);

            Document replacement = new Document("author_id", blog.getAuthorId())
                .append("title", blog.getTitle())
                .append("content", blog.getContent())
                .append("_id", new ObjectId(blogId));

            collection.replaceOne(eq("_id", new ObjectId(blogId)), replacement);

            System.out.println("Replaced, sending response");
            responseObserver.onNext(UpdateBlogResponse.newBuilder()
                .setBlog(adapt(replacement))
                .build());
            responseObserver.onCompleted();
            System.out.println("Update completed");
        } catch (StatusRuntimeException ex) {
            responseObserver.onError(ex);
        } catch (Exception ex) {
            responseObserver.onError(Status.NOT_FOUND
                .withDescription("The blog with given id was not found")
                .augmentDescription(ex.getLocalizedMessage())
                .asRuntimeException());
        }
    }

    @Override
    public void deleteBlog(DeleteBlogRequest request, StreamObserver<DeleteBlogResponse> responseObserver) {
        System.out.println("Received delete blog request");
        String blogId = request.getBlogId();

        try {
            DeleteResult result = collection.deleteOne(eq("_id", new ObjectId(blogId)));

            System.out.println("Deleted, sending response");
            responseObserver.onNext(DeleteBlogResponse.newBuilder()
                .setDeleted(result.getDeletedCount() > 0).build());
            responseObserver.onCompleted();
        } catch (Exception ex) {
            responseObserver.onError(Status.NOT_FOUND
                .withDescription(String.format("The blog with given id was not found [%s]", blogId))
                .augmentDescription(ex.getLocalizedMessage())
                .asRuntimeException());
        }
    }

    @Override
    public void listBlog(ListBlogRequest request, StreamObserver<ListBlogResponse> responseObserver) {
        System.out.println("Received list blog request");

        collection.find().iterator().forEachRemaining(document -> responseObserver.onNext(ListBlogResponse.newBuilder()
            .setBlog(adapt(document))
            .build()));

        responseObserver.onCompleted();
    }

    private Blog findById(String blogId) {
        System.out.println("Searching for blog...");
        Document result = collection.find(eq("_id", new ObjectId(blogId))).first();
        if (result == null) {
            System.out.println("Blog not found");
            throw Status.NOT_FOUND
                .withDescription(String.format("The blog with given id was not found [%s]", blogId))
                .asRuntimeException();
        }

        return adapt(result);
    }

    private Blog adapt(Document doc) {
        return Blog.newBuilder()
            .setId(doc.getObjectId("_id").toString())
            .setAuthorId(doc.getString("author_id"))
            .setTitle(doc.getString("title"))
            .setContent(doc.getString("content"))
            .build();
    }
}
