package org.elu.learn.grpc.calculator.client;

import com.proto.calculator.CalculatorServiceGrpc;
import com.proto.calculator.ComputeAverageRequest;
import com.proto.calculator.ComputeAverageResponse;
import com.proto.calculator.FindMaximumRequest;
import com.proto.calculator.FindMaximumResponse;
import com.proto.calculator.PrimeNumberDecompositionRequest;
import com.proto.calculator.SquareRootRequest;
import com.proto.calculator.SquareRootResponse;
import com.proto.calculator.SumRequest;
import com.proto.calculator.SumResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class CalculatorClient {
    public static void main(String[] args) throws InterruptedException {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 50052)
            .usePlaintext()
            .build();

        CalculatorServiceGrpc.CalculatorServiceBlockingStub client = CalculatorServiceGrpc.newBlockingStub(channel);

        // Unary
        SumRequest request = SumRequest.newBuilder()
            .setFirstNumber(2)
            .setSecondNumber(3)
            .build();

        SumResponse response = client.sum(request);

        System.out.println(String.format("%d + %d = %d ", request.getFirstNumber(), request.getSecondNumber(), response.getResult()));

        // Server streaming
        long number = 567890L;
        PrimeNumberDecompositionRequest primeNumberDecompositionRequest = PrimeNumberDecompositionRequest.newBuilder()
            .setNumber(number)
            .build();

        client.primeNumberDecomposition(primeNumberDecompositionRequest)
            .forEachRemaining(resp -> System.out.println(resp.getPrimeFactor()));

        // Client streaming
        CalculatorServiceGrpc.CalculatorServiceStub asyncClient = CalculatorServiceGrpc.newStub(channel);

        CountDownLatch latch = new CountDownLatch(1);

        StreamObserver<ComputeAverageRequest> requestObserver = asyncClient.computeAverage(new StreamObserver<ComputeAverageResponse>() {
            @Override
            public void onNext(ComputeAverageResponse value) {
                System.out.println("Received a response from the server: " + value.getAverage());
            }

            @Override
            public void onError(Throwable t) {
            }

            @Override
            public void onCompleted() {
                System.out.println("Server has completed");
                latch.countDown();
            }
        });

        for (int i = 0; i < 10000; i++) {
            requestObserver.onNext(ComputeAverageRequest.newBuilder()
                .setNumber(i)
                .build());
        }

        requestObserver.onCompleted();

        latch.await(3L, TimeUnit.SECONDS);

        // bi-directional streaming
        CountDownLatch latch1 = new CountDownLatch(1);

        StreamObserver<FindMaximumRequest> maxRequestObserver = asyncClient.findMaximum(new StreamObserver<FindMaximumResponse>() {
            @Override
            public void onNext(FindMaximumResponse value) {
                System.out.println("Received a maximum from the server: " + value.getMaximum());
            }

            @Override
            public void onError(Throwable t) {
                latch1.countDown();
            }

            @Override
            public void onCompleted() {
                System.out.println("Server has completed with maximum");
                latch1.countDown();
            }
        });
        Arrays.asList(3, 5, 17, 9, 8, 30, 12).forEach(num -> {
            System.out.println("Sending number: " + num);
            maxRequestObserver.onNext(FindMaximumRequest.newBuilder()
                .setNumber(num)
                .build());
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        maxRequestObserver.onCompleted();

        latch1.await(3L, TimeUnit.SECONDS);

        try {
            SquareRootResponse squareRootResponse = client.squareRoot(SquareRootRequest.newBuilder()
                .setNumber(-1)
                .build());
            System.out.println("got response: " + squareRootResponse.getNumberRoot());
        } catch (StatusRuntimeException ex) {
            System.out.println("Got an exception for square root: " + ex.getStatus());
            ex.printStackTrace();
        }

        channel.shutdown();
    }
}
