package org.elu.learn.grpc.greeting.client;

// import com.proto.dummy.DummyServiceGrpc;
import com.proto.greet.GreetEveryoneRequest;
import com.proto.greet.GreetEveryoneResponse;
import com.proto.greet.GreetManyTimesRequest;
import com.proto.greet.GreetRequest;
import com.proto.greet.GreetResponse;
import com.proto.greet.GreetServiceGrpc;
import com.proto.greet.GreetWithDeadlineRequest;
import com.proto.greet.GreetWithDeadlineResponse;
import com.proto.greet.Greeting;
import com.proto.greet.LongGreetRequest;
import com.proto.greet.LongGreetResponse;
import io.grpc.Deadline;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import io.grpc.stub.StreamObserver;

import javax.net.ssl.SSLException;
import java.io.File;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class GreetingClient {
    public static void main(String[] args) throws InterruptedException, SSLException {
        System.out.println("Hello, I'm gRPC client");

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 50051)
            .usePlaintext()
            .build();

        ManagedChannel securedChannel = NettyChannelBuilder.forAddress("localhost", 50051)
            .sslContext(GrpcSslContexts.forClient().trustManager(new File("ssl/ca.crt")).build())
            .build();

        System.out.println("Creating stub");
        // synchronous client
        // DummyServiceGrpc.DummyServiceBlockingStub syncClient = DummyServiceGrpc.newBlockingStub(channel);
        // asynchronous client
        // DummyServiceGrpc.DummyServiceFutureStub asyncClient = DummyServiceGrpc.newFutureStub(channel);

//        GreetServiceGrpc.GreetServiceBlockingStub greetClient = GreetServiceGrpc.newBlockingStub(channel);
        GreetServiceGrpc.GreetServiceBlockingStub greetClient = GreetServiceGrpc.newBlockingStub(securedChannel);

        Greeting greeting = Greeting.newBuilder()
            .setFirstName("Edu")
            .setLastName("Finn")
            .build();

        // Unary
        System.out.println("==== Unary request ====");
        GreetRequest request = GreetRequest.newBuilder()
            .setGreeting(greeting)
            .build();

        GreetResponse greetResponse = greetClient.greet(request);
        System.out.println(greetResponse.getResult());

        // Server streaming
        System.out.println("==== Server streaming ====");
        GreetManyTimesRequest manyRequest = GreetManyTimesRequest.newBuilder()
            .setGreeting(greeting)
            .build();

        greetClient.greetManyTimes(manyRequest)
            .forEachRemaining(response -> System.out.println(response.getResult()));

        // Client streaming
        System.out.println("==== Client streaming ====");
        GreetServiceGrpc.GreetServiceStub asyncClient = GreetServiceGrpc.newStub(channel);

        CountDownLatch latch = new CountDownLatch(1);

        StreamObserver<LongGreetRequest> requestObserver =  asyncClient.longGreet(new StreamObserver<LongGreetResponse>() {
            @Override
            public void onNext(LongGreetResponse value) {
                // get a response from the server
                System.out.println("Received a response from the server: " + value.getResult());
            }

            @Override
            public void onError(Throwable t) {
                // get an error from the server
            }

            @Override
            public void onCompleted() {
                // the server is done sending data
                System.out.println("Server has completed sending data");
                latch.countDown();
            }
        });

        requestObserver.onNext(LongGreetRequest.newBuilder()
            .setGreeting(greeting)
            .build());

        requestObserver.onNext(LongGreetRequest.newBuilder()
            .setGreeting(Greeting.newBuilder()
                .setFirstName("Janna")
                .build())
            .build());

        requestObserver.onNext(LongGreetRequest.newBuilder()
            .setGreeting(Greeting.newBuilder()
                .setFirstName("Arno")
                .build())
            .build());

        requestObserver.onNext(LongGreetRequest.newBuilder()
            .setGreeting(Greeting.newBuilder()
                .setFirstName("Lauri")
                .build())
            .build());

        requestObserver.onNext(LongGreetRequest.newBuilder()
            .setGreeting(Greeting.newBuilder()
                .setFirstName("Alisa")
                .build())
            .build());

        requestObserver.onNext(LongGreetRequest.newBuilder()
            .setGreeting(Greeting.newBuilder()
                .setFirstName("Karoliina")
                .build())
            .build());

        // tell the server that the client done sending data
        requestObserver.onCompleted();

        latch.await(5L, TimeUnit.SECONDS);

        // Bi-directional streaming
        System.out.println("==== Bi-directional streaming ====");
        CountDownLatch latch1 = new CountDownLatch(1);

        StreamObserver<GreetEveryoneRequest> greetEveryoneRequestStreamObserver = asyncClient.greetEveryone(new StreamObserver<GreetEveryoneResponse>() {
            @Override
            public void onNext(GreetEveryoneResponse value) {
                System.out.println("Response from server: " + value.getResult());
            }

            @Override
            public void onError(Throwable t) {
                latch1.countDown();
            }

            @Override
            public void onCompleted() {
                System.out.println("Server is done sending data");
                latch1.countDown();
            }
        });

        Arrays.asList("Edu", "Janna", "Arno", "Lauri", "Alisa", "Karoliina").forEach(name -> {
            System.out.println("Sending: " + name);
            greetEveryoneRequestStreamObserver.onNext(GreetEveryoneRequest.newBuilder()
                .setGreeting(Greeting.newBuilder()
                    .setFirstName(name)
                    .build())
                .build());
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        greetEveryoneRequestStreamObserver.onCompleted();

        latch1.await(3L, TimeUnit.SECONDS);

        try {
            System.out.println("Sending message with deadline 500ms");
            GreetWithDeadlineResponse greetWithDeadlineResponse = greetClient.withDeadline(Deadline.after(500, TimeUnit.MILLISECONDS))
                .greetWithDeadline(GreetWithDeadlineRequest.newBuilder()
                    .setGreeting(greeting)
                    .build());
            System.out.println("Response with deadline received: " + greetWithDeadlineResponse.getResult());
        } catch (StatusRuntimeException ex) {
            if (ex.getStatus() == Status.DEADLINE_EXCEEDED) {
                System.out.println("Deadline has been exceeded");
            } else {
                ex.printStackTrace();
            }
        }

        try {
            System.out.println("Sending message with deadline 100ms");
            GreetWithDeadlineResponse greetWithDeadlineResponse = greetClient.withDeadline(Deadline.after(100, TimeUnit.MILLISECONDS))
                .greetWithDeadline(GreetWithDeadlineRequest.newBuilder()
                    .setGreeting(greeting)
                    .build());
            System.out.println("Response with deadline received: " + greetWithDeadlineResponse.getResult());
        } catch (StatusRuntimeException ex) {
            if (ex.getStatus().getCode() == Status.DEADLINE_EXCEEDED.getCode()) {
                System.out.println("Deadline has been exceeded");
            } else {
                ex.printStackTrace();
            }
        }

        System.out.println("Shutdown channel");
        channel.shutdown();
    }
}
